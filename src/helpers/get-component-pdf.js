module.exports = ({ data }) => {
  return `/__convert-to-latex/latex/${data.root.page.component.name}--${data.root.page.version}.pdf`
}
