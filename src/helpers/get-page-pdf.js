module.exports = ({ data }) => {
  return data.root.page.url.replace(/\.html$/, '.pdf')
}
